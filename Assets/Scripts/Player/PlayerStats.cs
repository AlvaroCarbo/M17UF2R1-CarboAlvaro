using System;
using System.Collections;
using System.Collections.Generic;
using Animations;
using DefaultNamespace;
using Enums;
using Player;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private int _maxHealth;

    private AnimatorController _animatorController;

    private void Start()
    {
        _animatorController = GetComponent<AnimatorController>();

        _health = _maxHealth;
    }

    public void DecreaseHealth(int amount)
    {
        //if (_health <= 0) return;
        var newHealth = _health - amount;
        _health = newHealth < 0 ? 0 : newHealth;
        _animatorController.PlayAnim(true, _health <= 0 ? AnimStates.Death : AnimStates.TakeDamage, AnimLayers.Damage);
    }

    private void OnTriggerEnter(Collider other)
    {
        var damageable = other.GetComponent<IDamageable>();

        if (damageable != null)
        {
            DecreaseHealth(damageable.Damage());
        }
    }
    
}