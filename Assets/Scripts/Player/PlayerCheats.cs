using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class PlayerCheats : MonoBehaviour
{
    private Transform _playerTransform;
    private PlayerMovement _playerMovement;
    
    private void Start()
    {
        _playerTransform = GetComponent<Transform>();
        _playerMovement = GetComponent<PlayerMovement>();
    }

    public void TransformPosition(Transform myTransform)
    {
        _playerMovement.enabled = false;
        StartCoroutine(TransformPosition(myTransform.position));
    } 

    private IEnumerator TransformPosition(Vector3 myPosition)
    {
        _playerTransform.position = myPosition;
        yield return new WaitForSeconds(0.5f);
        _playerMovement.enabled = true;
    }
}
