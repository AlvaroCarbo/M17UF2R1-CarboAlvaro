using Items;
using UnityEngine;

namespace Player
{
    public class ItemSlotHandler : MonoBehaviour
    {
        [SerializeField] private GameObject currentPrefab;
        [SerializeField] private Transform parentOverride;
        [SerializeField] private ItemM17 currentItemM17;

        public ItemM17 CurrentItemM17 => currentItemM17;
        private void UnloadItemAndDestroy()
        {
            if (currentPrefab != null)
            {
                Destroy(currentPrefab);
                currentItemM17 = null;
            }
        }
        
        
        public void LoadItem(ItemM17 itemM17)
        {
            UnloadItemAndDestroy();
            
            if (itemM17 == null) return;

            GameObject model = Instantiate(itemM17.prefab);

            if (model != null)
            {
                model.transform.parent = parentOverride != null ? parentOverride : transform;

                model.transform.localPosition = Vector3.zero;
                model.transform.localRotation = Quaternion.identity;
                model.transform.localScale = Vector3.one;
            }

            currentPrefab = model;
            currentItemM17 = itemM17;
        }
    }
}
