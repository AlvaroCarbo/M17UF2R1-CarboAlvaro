using System;
using System.Collections;
using System.Collections.Generic;
using Items;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using Object = UnityEngine.Object;

namespace Player
{
    public class PlayerInventory : MonoBehaviour
    {
        [SerializeField] private List<ItemM17> items = new List<ItemM17>();
        [SerializeField] private ItemM17 currentItemM17;
        [SerializeField] private PostProcessController postProcessController;
        [SerializeField] private PlayerManager playerManager;
        private PlayerUIController _playerUIController;
        [SerializeField] private ItemSlotHandler _itemSlotHandler;
        [SerializeField] private ItemM17 key;
        public List<ItemM17> Items => items;

        private int _currentIndex = 0;

        private void Awake()
        {
            postProcessController = GameObject.FindWithTag("MainCamera").GetComponent<PostProcessController>();
            playerManager = GetComponent<PlayerManager>();
            _playerUIController = GetComponent<PlayerUIController>();
            _itemSlotHandler = GetComponentInChildren<ItemSlotHandler>();
        }

        private void Start()
        {
            if (currentItemM17 != null)
            {
                LoadItemOnSlot(currentItemM17);
            }
        }

        public void LoadItemOnSlot(ItemM17 itemM17)
        {
            _itemSlotHandler.LoadItem(itemM17);
        }

        public void UseCurrentItem()
        {
            if (currentItemM17 != null)
            {
                currentItemM17.Use(this);
            }
        }

        public void SetCurrentItem()
        {
            currentItemM17 = _itemSlotHandler.CurrentItemM17;
        }

        private void SetCurrentItem(ItemM17 itemM17)
        {
            currentItemM17 = itemM17;
        }


        public void AddItem(ItemM17 itemM17) => items.Add(itemM17);

        public void RemoveItem(ItemM17 itemM17) => items.Remove(itemM17);

        public void ChangePlayerEffect(PostProcessProfile profile, float duration)
            => postProcessController.ChangeProfile(profile, duration);


        private void OnTriggerEnter(Collider other)
        {
            var pickUp = other.GetComponent<CollectiblePickUp>();
            if (pickUp)
            {
                _playerUIController.ShowMessage(pickUp.Message);
            }
            
            if (other.CompareTag("Door"))
            {
                var hasKey = items.Contains(key);
                if (hasKey)
                {
                    other.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                    _playerUIController.ShowMessage("Congratulations! You have unlocked the door!");
                    AudioManager.Instance.PlayAudio(AudioManager.Instance.correctSound);
                    _playerUIController.ShowGameOverPanel();
                    _playerUIController.ShowWinButton();
                    _playerUIController.SetWinText();
                }
                else
                {
                    _playerUIController.ShowMessage("You need a key to open this door");
                    AudioManager.Instance.PlayAudio(AudioManager.Instance.wrongSound);
                }
            }
        }
        

        private void OnTriggerStay(Collider other)
        {
            if (playerManager.IsCollecting)
            {
                var pickUp = other.GetComponent<CollectiblePickUp>();
                if (pickUp)
                {
                    _playerUIController.ShowMessage("");
                    pickUp.ItemM17.Collect(this);

                    Destroy(pickUp.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var pickUp = other.GetComponent<CollectiblePickUp>();
            if (pickUp)
            {
                _playerUIController.ShowMessage("");
            }

            if (other.CompareTag("Door"))
            {
                _playerUIController.ShowMessage("");
            }
        }

        public void ChangeItem(float newValue)
        {
            _currentIndex += (int) newValue;
            _currentIndex = _currentIndex > items.Count - 1 ? 0 : _currentIndex < 0 ? items.Count - 1 : _currentIndex;
            SetCurrentItem(items[_currentIndex]);
            LoadItemOnSlot(items[_currentIndex]);
        }
        
        
    }
}