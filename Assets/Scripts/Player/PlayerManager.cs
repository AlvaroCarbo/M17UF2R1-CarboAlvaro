using Animations;
using Camera;
using Enums;
using Inputs;
using UnityEngine;

namespace Player
{
    public class PlayerManager : MonoBehaviour
    {
        [Header("Player Flags")] 
        [SerializeField] private bool _isInteracting;
        [SerializeField] private bool _isCrouching;
        [SerializeField] private bool _isDancing;
        [SerializeField] private bool _isAiming;
        [SerializeField] private bool _isCollecting;
        [SerializeField] private bool _isDead;

        [Header("Player Components")] 
        private PlayerMovement _playerMovement;
        private GameInput _gameInput;

        [Header("Animator Components")] 
        [SerializeField] private AnimatorController _animatorController;

        [Header("Camera Components")] 
        [SerializeField] private CameraManager _cameraManager;
    
        public bool IsInteracting => _isInteracting;
        public bool IsCollecting => _isCollecting;
        public bool IsCrouching => _isCrouching;
        public bool IsDead => _isDead;

        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
            _animatorController = GetComponent<AnimatorController>();
            _gameInput = GetComponent<GameInput>();
            _cameraManager = GameObject.FindWithTag("MainCamera").GetComponent<CameraManager>();
        }

        private void Start()
        {
            _isInteracting = false;
            _isCrouching = false;
            _isDancing = false;
            _isAiming = false;
            _isDead = false;
        }
    
        private void LateUpdate()
        {
            UpdateManagerFlags();
            HandleAnimations();
        
            if (_cameraManager is not null) HandlePlayerCamera();
        }

        private void UpdateManagerFlags()
        {
            _isInteracting = _animatorController.GetAnimatorBool(AnimParameters.IsInteracting);
            _isCrouching = _gameInput.Crouch;
            _isDancing = _animatorController.GetAnimatorBool(AnimParameters.Dance);
            _isAiming = _animatorController.GetAnimatorBool(AnimParameters.Aiming);
            _isCollecting = _animatorController.GetCurrentAnimState(
                (int) AnimLayers.Interact)
                .IsName(AnimStates.PickUp.ToString());
        }

        private void HandleAnimations()
        {
            _animatorController.SetAnimatorBool(AnimParameters.IsCrouching, _isCrouching);
            _animatorController.SetAnimatorFloat(AnimParameters.Horizontal, _gameInput.Movement.x);
            _animatorController.SetAnimatorFloat(AnimParameters.Vertical, _gameInput.Movement.y);
            _animatorController.SetAnimatorBool(AnimParameters.MoveInput, _gameInput.MoveInput);
            _animatorController.SetAnimatorFloat(AnimParameters.Time, Time.time - _gameInput.LastInputInTime);
        }

        private void HandlePlayerCamera()
        {
            _cameraManager.SetCamera(_isDancing ? CameraManager.CameraType.Dance 
                : _isAiming ? CameraManager.CameraType.Aim : CameraManager.CameraType.ThirdPersonFollow);
            /*if (_isDancing)
        {
            _cameraManager.SetCamera(CameraManager.CameraType.Dance);
        }     
        else if (_isAiming)
        {
            _cameraManager.SetCamera(CameraManager.CameraType.Aim);
        }
        else
        {
            _cameraManager.SetCamera(CameraManager.CameraType.ThirdPersonFollow);
        }*/
        }
    }
}