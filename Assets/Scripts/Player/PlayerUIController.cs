using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Player
{
    public class PlayerUIController : MonoBehaviour
    {
        public static PlayerUIController Instance;
        [SerializeField] private Image _playerCrossHair;
        [SerializeField] private TMP_Text infoText;
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private TMP_Text gameOverText;
        [SerializeField] private GameObject winButton;

        private void Awake()
        {
            Instance = this;
        }

        public void SetCrossHair(Sprite newSprite)
        {
            _playerCrossHair.sprite = newSprite;
        }

        public void SetCrossHairActive(bool isActive)
        {
            _playerCrossHair.gameObject.SetActive(isActive);
        }

        public void ShowMessage(string message)
        {
            infoText.text = message;
        }

        public void HideMessage()
        {
            infoText.text = "";
        }

        public void ShowWinButton()
        {
            winButton.SetActive(true);
        }

        public void ShowGameOverPanel()
        {
            try
            {
                gameOverPanel.SetActive(true);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public void SetWinText()
        {
            gameOverText.text = "You Win!";
        }

        public void HideGameOverPanel()
        {
            gameOverPanel.SetActive(false);
        }

        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void ShowGameObject(GameObject go)
        {
            go.SetActive(true);
        }

        public void HideGameObject(GameObject go)
        {
            go.SetActive(false);
        }

        public void DeathHandler()
        {
            StartCoroutine(DeathHandlerCoroutine());
        }

        private IEnumerator DeathHandlerCoroutine()
        {
            yield return new WaitForSeconds(3);
            ShowGameOverPanel();
        }
    }
}