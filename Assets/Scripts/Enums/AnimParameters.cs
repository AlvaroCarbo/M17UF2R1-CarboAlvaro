namespace Enums
{
    public enum AnimParameters
    {
        Speed,
        IsInteracting,
        IsCrouching,
        Jump,
        Fall,
        Grounded,
        Horizontal,
        Vertical,
        Time,
        MoveInput,
        IsRunning,
        Aiming,
        Shot,
        Dance,
        JumpCount
    }
    
    public enum AnimStates
    {
        Idle,
        WalkDirectionalBlend,
        RunDirectionalBlend,
        JumpPlace,
        Jump_01,
        Jump_02,
        PickUp,
        DodgeFront,
        DanceMove,
        DanceStatic,
        Aim,
        Death,
        TakeDamage
    }

    public enum AnimLayers
    {
        BaseLayer = 0,
        Crouch = 1,
        Jump = 2,
        Shot = 3,
        Interact = 4,
        Dance = 5,
        Damage = 6,
        Override = 7
    }

}