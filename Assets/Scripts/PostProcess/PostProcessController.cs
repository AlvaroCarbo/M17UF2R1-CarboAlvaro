using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : MonoBehaviour
{
    private PostProcessVolume _volume;
    [SerializeField] private PostProcessProfile defaultProfile;
    
    private void Awake()
    {
        _volume = GetComponent<PostProcessVolume>();
    }
    
    public void ChangeProfile(PostProcessProfile profile, float time = 0)
    {
        _volume.profile = profile;
        StartCoroutine(SetDefaultProfile(time));
    }

    private IEnumerator SetDefaultProfile(float time)
    {
        yield return new WaitForSeconds(time);
        _volume.profile = defaultProfile;
    }
}