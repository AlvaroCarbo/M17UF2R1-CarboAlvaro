using System;
using System.Collections;
using System.Collections.Generic;
using Behaviors;
using BehaviorTree;
using Unity.VisualScripting;
using UnityEngine;

public class DetectionCollider : MonoBehaviour
{
    NonPlayerCharacter m_Npc;

    private void Awake()
    {
        m_Npc = GetComponentInParent<NonPlayerCharacter>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            m_Npc.MyActivity = NavigationActivity.FindPlayer;
        }
    }
}
