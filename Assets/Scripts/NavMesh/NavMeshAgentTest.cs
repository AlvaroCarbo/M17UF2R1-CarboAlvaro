using Inputs;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshAgentTest : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Transform player;
    [SerializeField] private Transform targetPosition;

    private bool _isFollowingPlayer = true;

    private Animator _animator;
    private static readonly int Velocity = Animator.StringToHash("Velocity");
    
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }


    private void Update()
    {
        MoveTo(_isFollowingPlayer ? player.position : targetPosition.position);

        agent.speed = agent.remainingDistance > 5 ? 5f : 2.5f;

        _animator.SetFloat(Velocity, agent.velocity.magnitude);
    }

    public void SwapFlag() => _isFollowingPlayer = !_isFollowingPlayer;

    private void MoveTo(Vector3 position) => agent.SetDestination(position);
}