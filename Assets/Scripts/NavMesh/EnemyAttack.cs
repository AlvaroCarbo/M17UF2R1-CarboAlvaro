using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Animator _animator;
    private static readonly int Attack = Animator.StringToHash("Attack");

    private void Awake()
    {
        _animator = GetComponentInParent<Animator>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _animator.SetTrigger(Attack);
        }
    }
}