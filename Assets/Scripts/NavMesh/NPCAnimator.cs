using System;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine;
using UnityEngine.AI;

public class NPCAnimator : MonoBehaviour
{
    private Animator _animator;
    private NavMeshAgent _npc;
    private static readonly int Velocity = Animator.StringToHash("Velocity");
    private static readonly int Attack = Animator.StringToHash("Attack");

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _npc = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        _npc.speed = _npc.remainingDistance > 5 ? 5f : 2.5f;

        _animator.SetFloat(Velocity, _npc.velocity.magnitude);
    }
    
    public void AttackPlayer()
    {
        _animator.SetTrigger(Attack);
    }
    public void PlayAnim(AnimStates targetParameter, AnimLayers layer = 0)
    {
        //_animator.SetBool(AnimParameters.IsInteracting.ToString(), isInteracting);
        _animator.CrossFade(targetParameter.ToString(), 0.2f, (int) layer);
        _animator.applyRootMotion = true;
    }
    
}
