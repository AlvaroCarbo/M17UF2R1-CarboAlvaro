using DefaultNamespace;
using UnityEngine;

public class DamageableEnemy : MonoBehaviour, IDamageable
{
    [SerializeField] private float baseDamage;

    public int Damage() => (int) baseDamage;

    private void OnCollisionEnter(Collision collision)
    {
        var playerStats = collision.gameObject.GetComponent<PlayerStats>();
        
        if (playerStats != null)
        {
            playerStats.DecreaseHealth(Damage());
        }
    }
}
