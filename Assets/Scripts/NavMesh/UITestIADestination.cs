using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class UITestIADestination : MonoBehaviour
{
    [SerializeField] private TMP_Text destination;
    [SerializeField] private TMP_Text distance;
    [SerializeField] private TMP_Text velocityMagnitude;

    [SerializeField] private NavMeshAgent enemyAgent;

    private void Update()
    {
        destination.text = $"{enemyAgent.destination}";
        distance.text = $"{enemyAgent.remainingDistance}";
        velocityMagnitude.text = $"{enemyAgent.velocity.magnitude}";
    }
}
