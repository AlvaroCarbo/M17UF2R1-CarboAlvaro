using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Enums;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private int _maxHealth;

    private NPCAnimator _npcAnimator;

    private void Awake()
    {
        _npcAnimator = GetComponentInParent<NPCAnimator>();
        _health = _maxHealth;
    }

    private void DecreaseHealth(int amount)
    {
        //if (_health <= 0) return;
        var newHealth = _health - amount;
        _health = newHealth < 0 ? 0 : newHealth;
        _npcAnimator.PlayAnim(_health <= 0 ? AnimStates.Death : AnimStates.TakeDamage);
    }

    private void OnTriggerEnter(Collider other)
    {
        var damageable = other.GetComponent<DamageableFire>();
        if (damageable != null)
        {
            DecreaseHealth(damageable.Damage());
        }
    }
}