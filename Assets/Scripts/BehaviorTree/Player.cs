﻿using BehaviorTree;
using UnityEngine;
using WUG.BehaviorTreeVisualizer;

namespace Behaviors
{
    public class Player : MonoBehaviour
    {

        private void OnTriggerEnter(Collider other)
        {
            if (GameManager.Instance.NPC.MyActivity == NavigationActivity.FindPlayer)
            {
                GameManager.Instance.FollowPlayer(this.gameObject);
            }
        }
    }
}
