using Behaviors;
using UnityEngine;
using WUG.BehaviorTreeVisualizer;

namespace BehaviorTree.Condition
{
    public class ArePlayersNearBy : Behaviors.Condition.Condition
    {
        private float m_DistanceToCheck;
        
        public ArePlayersNearBy(float maxDistance) : base($"Are Players within {maxDistance}f?") 
        { 
            m_DistanceToCheck = maxDistance; 
        }

        protected override void OnReset() { }
        protected override NodeStatus OnRun()
        {
            //Check for references
            if (GameManager.Instance == null || GameManager.Instance.NPC == null)
            {
                StatusReason = "GameManager and/or NPC is null";
                return NodeStatus.Failure;
            }
            //Get the closest item
            GameObject player = GameManager.Instance.GetClosestPlayer();
            //Check to see if something is close by
            if (player == null)
            {
                StatusReason = "No players near by";
                return NodeStatus.Failure;
            }
            else if (Vector3.Distance(player.transform.position, 
                         GameManager.Instance.NPC.transform.position) > m_DistanceToCheck)
            {
                StatusReason = $"No players within range of {m_DistanceToCheck} meters";
                return NodeStatus.Failure;
            }
            return NodeStatus.Success;
        }
    }
}