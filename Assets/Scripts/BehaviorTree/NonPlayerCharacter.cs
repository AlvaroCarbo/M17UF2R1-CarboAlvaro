﻿using System.Collections;
using Behaviors;
using Behaviors.Condition;
using BehaviorTree.Actions;
using BehaviorTree.Condition;
using UnityEngine;
using UnityEngine.AI;
using WUG.BehaviorTreeVisualizer;

namespace BehaviorTree
{
    public enum NavigationActivity
    {
        Waypoint, 
        FindPlayer
    }

    public class NonPlayerCharacter : MonoBehaviour, IBehaviorTree
    {
        public NavMeshAgent MyNavMesh { get; private set; }
        public NodeBase BehaviorTree { get; set; }
        public NavigationActivity MyActivity { get; set; }

        private Coroutine m_BehaviorTreeRoutine;
        private YieldInstruction m_WaitTime = new WaitForSeconds(.1f);


        private void Start()
        {
            MyNavMesh = GetComponent<NavMeshAgent>();
            MyActivity = NavigationActivity.Waypoint;
            GenerateBehaviorTree();
            
            if (m_BehaviorTreeRoutine == null && BehaviorTree != null)
            {
                m_BehaviorTreeRoutine = StartCoroutine(RunBehaviorTree());
            }
        }

        private void OnDestroy()
        {
            if (m_BehaviorTreeRoutine != null)
            {
                StopCoroutine(m_BehaviorTreeRoutine);
            }
        }

        private IEnumerator RunBehaviorTree()
        {
            while (enabled)
            {
                if (BehaviorTree == null)
                {
                    $"{this.GetType().Name} is missing Behavior Tree. Did you set the BehaviorTree property?".BTDebugLog();
                    continue;
                }

                (BehaviorTree as Node).Run();

                yield return m_WaitTime;
            }
        }

        private void GenerateBehaviorTree()
        {
            BehaviorTree = new Selector("Control NPC",
                                new Sequence("Follow Player",
                                    new IsNavigationActivityTypeOf(NavigationActivity.FindPlayer),
                                    new Selector("Look for or move to player",
                                        new Sequence("Look for players",
                                            new Inverter("Inverter",
                                                new ArePlayersNearBy(10f)),
                                            new SetNavigationActivityTo(NavigationActivity.Waypoint)),
                                        new Sequence("Navigate to Player",
                                            new NavigateToDestination()))),
                                new Sequence("Move to Waypoint",
                                    new IsNavigationActivityTypeOf(NavigationActivity.Waypoint),
                                    new NavigateToDestination(),
                                    new Timer(2f,
                                        new Idle()),
                                    new SetNavigationActivityTo(NavigationActivity.FindPlayer)));
        }

        public void ForceDrawingOfTree()
        {
            if (BehaviorTree == null)
            {
                $"Behavior tree is null - nothing to draw.".BTDebugLog();
            }

            BehaviorTreeGraphWindow.DrawBehaviorTree(BehaviorTree, true);
        }
                
    }
}
