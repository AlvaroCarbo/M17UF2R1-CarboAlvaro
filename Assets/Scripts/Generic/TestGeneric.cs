using UnityEngine;

namespace Generic
{
    public class TestGeneric : MonoBehaviour
    {

        public T GenericMethod<T>(T a) where T : MonoBehaviour
        {
            return a;
        }
    
        public class MyClass<T>
        {
            public T Value;
            public string Name;
        }

        public class Car
        {
            public string Name;
            public int Speed;
        }

        public int x;
        public string y;
        public Car z;
    
        private void Start()
        {
            var myInt = new MyClass<int>
            {
                Value = 10,
                Name = "MyInt"
            };
            Debug.Log(myInt.Value);
            Debug.Log(myInt);
            Debug.Log(myInt.Name);
            x = myInt.Value;

            var myString = new MyClass<string>
            {
                Value = "Hello"
            };
            Debug.Log(myString.Value);
            y = myString.Value;

            var myCar = new MyClass<Car>
            {
                Value = new Car()
                {
                    Name = "BMW",
                    Speed = 200
                }
            };
            Debug.Log(myCar.Value);
            Debug.Log(myCar.Value.Name);
            z = myCar.Value;
            Debug.Log(z.Name);
            Debug.Log(z.Speed);
        }
    }
}