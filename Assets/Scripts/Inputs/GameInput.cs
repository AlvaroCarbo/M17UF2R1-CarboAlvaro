using System;
using Animations;
using Enums;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace Inputs
{
    public class GameInput : MonoBehaviour
    {
        [SerializeField] private AnimatorController animatorController;
        [SerializeField] private PlayerInventory playerInventory;
        [SerializeField] private NavMeshAgentTest navMeshAgentTest;

        private Vector2 _look;
        private Vector2 _movement;
        private bool _moveInput;
        private bool _jump;
        private bool _run;
        private bool _crouch;
        private bool _roll;
        private bool _pickUp;
        private int _jumpCount;
        private float _lastInputInTime;
        private bool _dance;
        private bool _attack;
        private bool _aim;

        public bool MoveInput => _moveInput;
        public Vector2 Movement => _movement;
        public Vector2 Look => _look;
        public bool Run => _run;
        public bool Crouch => _crouch;
        public float LastInputInTime => _lastInputInTime;
        public bool Jump => _jump;
        public int JumpCount => _jumpCount;

        private void Awake()
        {
            animatorController = GetComponent<AnimatorController>();
            playerInventory = GetComponent<PlayerInventory>();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            _movement = context.ReadValue<Vector2>();
            _moveInput = context.performed;
            if (_movement.magnitude == 0)
            {
                _moveInput = false;
            }

            _lastInputInTime = Time.time;
        }

        public void OnLook(InputAction.CallbackContext context) => _look = context.ReadValue<Vector2>();

        public void OnJump(InputAction.CallbackContext context)
        {
            _jump = context.performed;
            _lastInputInTime = Time.time;


            if (!_jump || !animatorController.GetAnimatorBool(AnimParameters.Grounded)) return;
            _jumpCount += _jumpCount > 0 ? -1 : 1;
            animatorController.SetAnimatorFloat(AnimParameters.JumpCount, _jumpCount);
        }

        public void OnRun(InputAction.CallbackContext context)
        {
            _run = context.performed;
            _lastInputInTime = Time.time;
        }

        public void OnCrouch(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _crouch = !_crouch;
            }

            _lastInputInTime = Time.time;
        }

        public void OnPickUp(InputAction.CallbackContext context)
        {
            _pickUp = context.performed;
            _lastInputInTime = Time.time;

            if (!_pickUp || !animatorController.GetAnimatorBool(AnimParameters.Grounded) ||
                animatorController.GetAnimatorBool(AnimParameters.IsInteracting)) return;

            animatorController.PlayAnim(true, AnimStates.PickUp, AnimLayers.Interact);
        }

        public void OnRoll(InputAction.CallbackContext context)
        {
            _roll = context.performed;
            _lastInputInTime = Time.time;

            if (!_roll || !animatorController.GetAnimatorBool(AnimParameters.Grounded)) return;

            animatorController.PlayAnim(true, AnimStates.DodgeFront, AnimLayers.Interact);
        }

        public void OnDance(InputAction.CallbackContext context)
        {
            _dance = context.performed;
            _lastInputInTime = Time.time;

            if (!_dance || !animatorController.GetAnimatorBool(AnimParameters.Grounded)) return;

            switch (Random.Range(0, 2))
            {
                case 0:
                    animatorController.PlayAnim(false, AnimStates.DanceMove, AnimLayers.Dance);
                    animatorController.SetAnimatorBool(AnimParameters.IsCrouching, true);
                    break;
                case 1:
                    animatorController.PlayAnim(true, AnimStates.DanceStatic, AnimLayers.Dance);
                    break;
            }

            animatorController.SetAnimatorBool(AnimParameters.Dance, true);
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            _attack = context.performed;
            _lastInputInTime = Time.time;

            animatorController.SetAnimatorBool(AnimParameters.Shot, _attack);
            if (_attack)
            {
                playerInventory.UseCurrentItem();
            }
        }

        public void OnAim(InputAction.CallbackContext context)
        {
            _aim = context.performed;
            _lastInputInTime = Time.time;
            animatorController.SetAnimatorBool(AnimParameters.Aiming, _aim);
        }

        public void OnChangeItem(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                Debug.Log(context.ReadValue<float>());
                playerInventory.ChangeItem(context.ReadValue<float>());
            }
        }

        public void OnChangeEnemyDestination(InputAction.CallbackContext context)
        {
            if (!context.performed) return;
            try
            {
                navMeshAgentTest.SwapFlag();
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}