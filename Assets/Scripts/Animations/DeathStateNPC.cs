using System.Collections;
using System.Collections.Generic;
using Behaviors;
using BehaviorTree;
using UnityEngine;
using UnityEngine.AI;

public class DeathStateNPC : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<NavMeshAgent>().enabled = false;
        animator.gameObject.GetComponent<NonPlayerCharacter>().enabled = false;   
        animator.gameObject.GetComponent<NPCAnimator>().enabled = false;   
        animator.gameObject.GetComponent<CapsuleCollider>().enabled = false;   
        animator.gameObject.GetComponentInChildren<TakeDamage>().enabled = false;   
    }
}
