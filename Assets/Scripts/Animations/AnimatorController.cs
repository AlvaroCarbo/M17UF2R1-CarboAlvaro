using Enums;
using UnityEngine;

namespace Animations
{
    public class AnimatorController : MonoBehaviour
    {
        private Animator _animator;
    
    
        private void Awake() => _animator = GetComponent<Animator>();
    

        public void SetAnimatorBool(AnimParameters param, bool value) => _animator.SetBool(param.ToString(), value);
    
        public void SetAnimatorBool(string param, bool value) => _animator.SetBool(param, value);
    
        public bool GetAnimatorBool(AnimParameters param) => _animator.GetBool(param.ToString());
    
        public bool GetAnimatorBool(string param) => _animator.GetBool(param);
    
    
        public void SetAnimatorFloat(AnimParameters param, float value) => _animator.SetFloat(param.ToString(), value);
    
        public void SetAnimatorFloat(string param, float value) => _animator.SetFloat(param, value);
    
        public float GetAnimatorFloat(AnimParameters param) => _animator.GetFloat(param.ToString());
    
        public float GetAnimatorFloat(string param) => _animator.GetFloat(param);

        public AnimatorStateInfo GetCurrentAnimState(int layer = 0)  => _animator.GetCurrentAnimatorStateInfo(layer);


        public void SetAnimatorTrigger(AnimParameters param) => _animator.SetTrigger(param.ToString());
    
        public void SetAnimatorTrigger(string param) => _animator.SetTrigger(param);
    
        public void ResetAnimatorTrigger(AnimParameters param) => _animator.ResetTrigger(param.ToString());
    
        public void ResetAnimatorTrigger(string param) => _animator.ResetTrigger(param);
  
    
        public void PlayAnim(bool isInteracting, AnimStates targetParameter, AnimLayers layer = 0)
        {
            if (GetAnimatorBool(AnimParameters.IsInteracting)) return;
        
            _animator.SetBool(AnimParameters.IsInteracting.ToString(), isInteracting);
            _animator.CrossFade(targetParameter.ToString(), 0.2f, (int) layer);
            _animator.applyRootMotion = true;
        }
    
    
        public void DisableDance() => _animator.SetBool(AnimParameters.Dance.ToString(), false);
    
    }
}