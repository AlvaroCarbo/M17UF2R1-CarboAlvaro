using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class DeathState : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        try
        {
            PlayerUIController.Instance.DeathHandler();
            animator.gameObject.GetComponent<CharacterController>().enabled = false;
            animator.gameObject.GetComponent<PlayerMovement>().enabled = false;
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);
        }
    }
}