using UnityEngine;
using UnityEngine.AI;

namespace Animations
{
    public class DamageStateNPC : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetComponent<NavMeshAgent>().isStopped = true;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetComponent<NavMeshAgent>().isStopped = false;
        }
}
}