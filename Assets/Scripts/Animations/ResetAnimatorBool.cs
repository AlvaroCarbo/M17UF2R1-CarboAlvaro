using UnityEngine;

namespace Animations
{
    public class ResetAnimatorBool : StateMachineBehaviour
    {
        public string parameter;
        public bool resetValue;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.applyRootMotion = false;
            animator.SetBool(parameter, resetValue);
        }
    }
}