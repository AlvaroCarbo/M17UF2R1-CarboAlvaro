using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ComboStates : StateMachineBehaviour
{
    private static readonly int AttackCounter = Animator.StringToHash("AttackCounter");
    public int comboCounter;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger(AttackCounter, comboCounter);
        animator.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
    } 
    
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<NavMeshAgent>().isStopped = false;
    }
}