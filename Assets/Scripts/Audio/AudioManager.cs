using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private AudioSource _audioSource;

    public AudioClip pickUpItem;  
    public AudioClip wrongSound;
    public AudioClip correctSound;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudio(AudioClip audioClip)
    {
        _audioSource.PlayOneShot(audioClip);
    }

    public void AtPoint(AudioClip clip)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }
}