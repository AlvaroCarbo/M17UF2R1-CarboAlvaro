using UnityEngine;

namespace Camera
{
    [CreateAssetMenu(fileName = "New Game Camera", menuName = "Camera/Game Camera", order = 1)]
    public class GameCameraSObject : ScriptableObject
    {
        public CameraManager.CameraType cameraType;
        [Range(0,1)] public float sensitivity;
        public Sprite crossHairSprite;
        public Sprite crossHairHit;
    }
}