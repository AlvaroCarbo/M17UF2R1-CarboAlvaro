using System;
using System.Collections.Generic;
using Cinemachine;
using Player;
using UnityEngine;
using UnityEngine.Playables;

namespace Camera
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private PlayableDirector _playableDirector;

        [Header("Cameras")] [Header("Third Person Follow")] [SerializeField]
        private CinemachineVirtualCamera _thirdPersonCamera;

        [SerializeField] private GameCameraSObject _thirdPersonCameraData;
        [Header("Aim")] [SerializeField] private CinemachineVirtualCamera _aimCamera;
        [SerializeField] private GameCameraSObject _aimCameraData;
        [Header("Dance")] [SerializeField] private CinemachineVirtualCamera _danceCamera;
        [SerializeField] private GameCameraSObject _danceCameraData;

        private Dictionary<CameraType, GameCamera> _cameras;

        [SerializeField] private CameraType _currentCameraType;

        [SerializeField] private PlayerMovement _playerMovement;

        [SerializeField] private PlayerUIController _playerUIController;


        public enum CameraType
        {
            ThirdPersonFollow,
            Aim,
            Dance
        }


        private void Start()
        {
            _playableDirector = GameObject.FindWithTag("PlayableDirector").GetComponent<PlayableDirector>();
            _playerMovement = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
            _playerUIController = GameObject.FindWithTag("Player").GetComponent<PlayerUIController>();

            _cameras = new Dictionary<CameraType, GameCamera>
            {
                {
                    CameraType.ThirdPersonFollow,
                    new GameCamera(_thirdPersonCamera, _thirdPersonCameraData.sensitivity,
                        _thirdPersonCameraData.crossHairSprite)
                },
                {
                    CameraType.Aim,
                    new GameCamera(_aimCamera, _aimCameraData.sensitivity, _aimCameraData.crossHairSprite)
                },
                {
                    CameraType.Dance,
                    new GameCamera(_danceCamera, _danceCameraData.sensitivity, _danceCameraData.crossHairSprite)
                }
            };
        }


        public void SetCamera(CameraType newCameraType)
        {
            if (_playableDirector.state == PlayState.Playing || newCameraType == _currentCameraType)
            {
                return;
            }

            _currentCameraType = newCameraType;

            try
            {
                foreach (var cameraType in _cameras.Keys)
                {
                    _cameras[cameraType].VirtualCamera.enabled = cameraType == _currentCameraType;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            _playerMovement.SetSensitivity(_cameras[_currentCameraType].Sensitivity);

            try
            {
                _playerUIController.SetCrossHair(_cameras[_currentCameraType].CrossHairSprite);
                _playerUIController.SetCrossHairActive(newCameraType != CameraType.Dance);
            }
            catch (NullReferenceException e)
            {
                Debug.Log(e.Message);
            }


            if (newCameraType == CameraType.Dance)
            {
                _playableDirector.Play();
            }
        }
    }
}