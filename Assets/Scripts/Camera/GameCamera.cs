using Cinemachine;
using UnityEngine;

namespace Camera
{
    public struct GameCamera
    {
        private CinemachineVirtualCamera _virtualCamera;
        private float _sensitivity;
        private Sprite _crossHairSprite;

        public GameCamera(CinemachineVirtualCamera virtualCamera, float sensitivity,
            Sprite crossHairSprite)
        {
            _virtualCamera = virtualCamera;
            _sensitivity = sensitivity;
            _crossHairSprite = crossHairSprite;
        }

        public CinemachineVirtualCamera VirtualCamera => _virtualCamera;
        public float Sensitivity => _sensitivity;
        public Sprite CrossHairSprite => _crossHairSprite;
    }
}