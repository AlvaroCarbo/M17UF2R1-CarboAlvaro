
using Player;

namespace Items
{
    public interface ICollectible
    {
        public void Collect(PlayerInventory playerInventory);
    }
}
