using System;
using UnityEngine;

namespace Items
{
    public class LightController : MonoBehaviour
    {
        [SerializeField] private new Light light;
        [SerializeField] private bool isOn;
        [SerializeField] [Range(0,1)] private float intensity;
        [SerializeField] private Flashlight flashlight;

        private void Awake()
        {
            light = GetComponent<Light>();
        }

        private void EnableLight(bool flag)
        {
            isOn = flag;
            light.enabled = flag;
        }

        private void LateUpdate()
        {
            if (flashlight.IsOn == isOn) return;

            EnableLight(flashlight.IsOn);
        }
    }
}