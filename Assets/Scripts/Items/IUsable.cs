using Player;

namespace Items
{
    public interface IUsable
    {
        void Use(PlayerInventory playerInventory);
    }
}