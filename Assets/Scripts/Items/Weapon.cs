using Player;
using UnityEngine;

namespace Items
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Item/Weapon", order = 1)]
    public class Weapon : ItemM17
    {

        public override void Collect(PlayerInventory playerInventory)
            {
                base.Collect(playerInventory);
                Debug.Log("weapon collected");
            } 

        
            public override void Use(PlayerInventory playerInventory)
            {
                
            }
        
    }
}