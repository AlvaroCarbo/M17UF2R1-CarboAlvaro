using System.Collections;
using Player;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Items
{
    [CreateAssetMenu(fileName = "Mushroom", menuName = "Item/Mushroom", order = 1)]
    public sealed class Mushroom : ItemM17
    {
        [SerializeField] private PostProcessProfile postProcessProfile;
        [SerializeField] private float duration = 5f;

        public override void Collect(PlayerInventory playerInventory)
        {
            EatMushroom(playerInventory);
            ItemsUIController.Instance.SetUIItem(this);
            AudioManager.Instance.PlayAudio(AudioManager.Instance.pickUpItem);
            /*base.Collect(playerInventory)*/;
            Debug.Log("Mushroom collected");
        } 

        
        public override void Use(PlayerInventory playerInventory)
        {
            base.Use(playerInventory);
        }

        private void EatMushroom(PlayerInventory playerInventory)
        {
            playerInventory.ChangePlayerEffect(postProcessProfile, duration);
        }
    }
}