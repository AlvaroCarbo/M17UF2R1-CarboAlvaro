using System;
using DefaultNamespace;
using UnityEngine;

public class DamageableFire : MonoBehaviour, IDamageable
{
    [SerializeField] private float baseDamage;

    public int Damage() => (int)baseDamage;
}