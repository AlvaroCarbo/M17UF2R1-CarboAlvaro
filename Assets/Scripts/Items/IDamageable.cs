namespace DefaultNamespace
{
    public interface IDamageable
    {
        public int Damage();
    }
}