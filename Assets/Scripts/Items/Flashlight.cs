using System;
using System.Collections.Generic;
using Player;
using UnityEngine;

namespace Items
{
    [CreateAssetMenu(fileName = "Flashlight", menuName = "Item/Flashlight", order = 1)]
    public class Flashlight : ItemM17
    {
        [SerializeField] private bool isOn;
        [SerializeField] private Light flashLight;
        
        public bool IsOn => isOn;
        public override void Collect(PlayerInventory playerInventory)
        {
            base.Collect(playerInventory);
            /*playerInventory.LoadItemOnSlot(this);
            playerInventory.SetCurrentItem();*/
            flashLight = prefab.GetComponent<Light>();
            Debug.Log("Flashlight collected");
        } 

        
        public override void Use(PlayerInventory playerInventory)
        {
            isOn = !isOn;            
        }
        

        /*public override void SetPrefab()
        {
            
        }*/
    }
}