using Player;
using UnityEngine;

namespace Items
{
    public class ItemM17 : ScriptableObject, ICollectible, IUsable
    {
        public GameObject prefab;
        public Sprite itemIcon;
        public bool isEspecial;

        public virtual void Collect(PlayerInventory playerInventory)
        {
            playerInventory.AddItem(this);
            playerInventory.LoadItemOnSlot(this);
            playerInventory.SetCurrentItem();

            AudioManager.Instance.PlayAudio(AudioManager.Instance.pickUpItem);
            if (isEspecial)
            {
                ItemsUIController.Instance.SetEspecialItem(this);
            }
            else
            {
                ItemsUIController.Instance.SetUIItem(this);
            }
        }

        public virtual void Use(PlayerInventory playerInventory) => playerInventory.RemoveItem(this);
    }
}