using UnityEngine;

namespace Items
{
    public class CollectiblePickUp : MonoBehaviour
    {
        [SerializeField] private ItemM17 itemM17;
        [SerializeField] private string message;
        public ItemM17 ItemM17 => itemM17;
        public string Message => message;
    }
}