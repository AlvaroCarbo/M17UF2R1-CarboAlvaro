using System;
using System.Collections;
using System.Collections.Generic;
using Items;
using Player;
using UnityEngine;
using UnityEngine.UI;

public class ItemsUIController : MonoBehaviour
{
    public static ItemsUIController Instance;

    [SerializeField] private GameObject[] itemsUIHolder;
    [SerializeField] private GameObject[] itemsUI;

    private PlayerInventory _playerInventory;

    private int _nextSlotToFill = 0;

    private int _specialItemIndex = 6;

    private void Awake()
    {
        Instance = this;
        _playerInventory = GameObject.FindWithTag("Player").GetComponent<PlayerInventory>();
    }

    void Start()
    {
        itemsUIHolder = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            itemsUIHolder[i] = transform.GetChild(i).gameObject;
        }

        itemsUI = new GameObject[itemsUIHolder.Length];
        for (int i = 0; i < itemsUIHolder.Length; i++)
        {
            itemsUI[i] = itemsUIHolder[i].transform.GetChild(0).gameObject;
        }
    }

    public void SetUIItem(ItemM17 itemM17)
    {
        if (_nextSlotToFill > itemsUI.Length - 1)
        {
            Debug.Log("No more space in inventory");
            return;
        }

        itemsUI[_nextSlotToFill].GetComponent<Image>().sprite = itemM17.itemIcon;
        _nextSlotToFill++;
    }
    
    public void SetEspecialItem(ItemM17 itemM17)
    {
        itemsUI[_specialItemIndex].GetComponent<Image>().sprite = itemM17.itemIcon;
    }
}