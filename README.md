# M17UF2R1-CarboGarciaAlvaro


## Controladors d'Accions:

- **Move**: WASD,
- **Look**: Mouse,
- **Aim**: Right Click Mouse,
- **Shot/Use**: Left Click Mouse,
- **Run**: Left Shift,
- **Jump**: Space,
- **Crouch**: C,
- **Roll**: Left Alt,
- **Interact**/**PickUp**: E,
- **Dance**: F,
- **Change Item**: Scroll Mouse (y-axis),
- **Enter**: Change NPC target position (R1.3.1 Scene)

##M17UF4R1.4 - Survivor: enemics i IA
**DONE:**

**R1.4.1 - Programar Nav Agent enemic (25%)**
- Crear un nivel MOLT bàsic petit on fer les probes de l'enemic. Del mateix Standard Assets per exemple. El nivell ha de tenir un mínim de:
  - Un terra
  - Una rampa amb un segon pis
  - Una paret
- Carregar/crear en un prefab enemic amb les accions:
  - Idle
  - Caminar/Correr
  - Atacar
  - **(Extra)** Morir
  - **(Extra-Personal)** Rebre dany
- Carrgar el player.
- Configurar un Nav Mesh de l'escenari i el Nav Agent de l'enemic.
- Al executar el joc l'enemic anirà cap a el player i el perseguirà.
  - Si s'apreta la tecla INTRO:
    - Si l'enemic persegueix a player --> anirà una posició de l'escenari preconfigurada.
    - Si l'enemic està de camí a la posició preconfigurada o ja ha arribat --> Perseguirà al player.
- Si l'enemic arriba a la posició l'animació ha de passar a IDLE.
- Si l'enemic arriba a estar a prop del player activa l'animació d'atacar.
                
**R1.4.2 - Enemic a l'escena principal (20%)**

- Crear un Nav mesh amb un dels terrains:
  - L'enemic no pot atravesar parets ni arbres.
  - **(Extra)** Si teniu construccions accecibles pel player com coves o cases, l'enemic també ha de poder accedir.
  - **(Extra-Personal)** S'ha afegit el package NavMesh Components per fer ús dels NavLinks entre d'altres components.

**R1.4.3 - Màquina d'estats (60%)**
- Afegir al Enemic:
  - Un collider per saber el dany. Ha d'envoltar el cos de l'enemic. No trigger. (Es possible que ja estigui fet depenent del asset emprat)
  - Un collider (Sphere preferiblement) que ha d'anar per davant de l'enemic. Será la zona de la mirada o zona de visió. Podeu afegirla al cap o root. Aquest ha de ser trigger.
  - Per a poder diferenciar entre un collider o un altre el codi de detecció ha d'estar al pare, a l'objecte base de l'enemic.

- Crear màquina d'estats per a l'enemic **(Behaviour Tree)**: 
  - Crear un **behauvior tree**: podeu fer servir un asset amb la implementació del behauvior tree, l'haureu de configurar i desenvolupar els diferents comportaments.
- Comportament de l'enemic:
  - Patrol State: L'enemic comença en un estat incial patrulla entre un mínim de 3 punts
  - Persecute State:
    - Si player entra al "camp de visió" de l'enemic, aquest el seguiex.
    - Mentres no estigui suficientment aprop l'enemic seguirà en aquest estat.
    - Si la distància es suficcient per atacar, canvia l'estat a Attack.
- Attack State:
  - Si aconsegueix estar molt aprop del player, l'enemic ataca.
  - Si la distància amb el player no és suficient passarà a Persecute State.
- **(Extra)** Alert State:
  - Condició d'entrada: Si l'enemic surt del estat Persecute State per perdre de vista al player.
  - Comportament: caminarà en amb el patrol random (o prefet) per la zona on ha perdut al usuari durant un temps determinat. Està cercant al player.
  - Condicions de sortida:
    - L'enemic ha detectat al player --> Persecute State
    - L'enemi ha esgotat el temps o recorregut de cerca --> Patrol State.
- **(Extra)** Mort de l'enemic (Die State):
  - Afegir a l'escena un dels elements que poden fer mal al jugador
  - Condició d'entrara: Si l'enemic toca un d'aquests elements ha de tenir un Die State.
  - Comportament: l'enemic realitza l'animació de Morir. I queda quiet a la zona.
  - **(Extra-Personal)** Si l'enemic toca un d'aquests elements ha de restar-li vida i fer una animació de mal, quan la vida es igual o inferior a zero, l'enemic entra en el Die State anterior.
  
## M17UF3R1.3 - Gameplay

**DONE:**

**R1.3.1 - Elements jugables:**

- Duplicar l'escena anterior R1.2.1 i anomenar-la R1.3.1 
- Per aquest exercici heu de fer servir herencia i/o interfícies
- Crear fins a 3 objectes(Prefabs) que el jugador haurà d'agafar distrubuits per una zona del escenari nou (R1.3.1).
- Aquests objectes no han d'estar "baked" en llums. Tindram llum calculada.
- Ha de ser col·leccionats per l'usuari. Guardar en un Scriptable Object els objectes agafats.
- Al agafar-los el joc emetrà un so.
- Al agafar-los el personatge jugador ha de mostrar l'objecte adderit/equipat en ell. Per exemple: en cas d'una espasa, doncs la porta penjada a l'esquena.
- Resaltar els objectes amb algun efecte que permeti direfenciar-ho de la resta: aura, particules, animació, etc
- Crear un 4art objecte del mateix tipus amb una interactivitat diferent: desbloqueja una zona: per exemple una clau que obra una porta.
- Crear dos tipus diferents d'objecte que fan mal i eliminen al jugador (Podeu fer servir interficie tipus IDamagable). Al tocar-los la partida s'acaba i s'obre la UI de Game Over (R1.3.3).
- **(Extra)** Si el jugador ha mort, activa una animació on el personatge està morint. Un cop acabada salta la UI

**R1.3.2 - Final de nivell:**
- Crear una zona de final de nivell. Per exemple: una porta, un portal, l'entrada al poble.
- Si l'usuari no te el 4art objecte no podrà accedir.
- En cas de no poder accedir i que l'usuari ho provi un text indicara que ha de buscar la clau.
- Si l'usuari prova de pasar i te la clau es finalitza partida i es motra pantalla de Game Over (R1.3.3).
- **(Extra)** En cas de no poder accedir i que l'usuari ho provi el joc fara un so.
- **(Extra)** En cas de poder accedir fer soroll de victoria.

**R1.3.3 - UI:**
- Crear una escena amb una UI d'inici de joc:
  - Escrit amb les instruccions de joc
  - Mostrar un panell amb els objectes a col·leccionar.
  - Botó start: carrega l'escena R1.3.1
- Crear UI de Game Over:
  - Botó de reiniciar nivell.
  - **(Extra)** Mostrar objectes que s'han agafat.   
  - Mostra el missatge si s'ha guanyat o s'ha perdut
- HUD:
  - Panell amb els objectes col·leccionats. Si no s'han agafat i haurà una silueta de l'objecte. Si s'ha agafat el mostrarà.



## R1.2 Entrega:
- Diferents biomes que pot recorrer el jugador.
- Diferens props / edificis / estructures / llocs d'interes repartits pel terreny.
- Decoració d'interiors.
- Afegits diferents items que pot agafar el jugador: power ups, armes...
- Modificació de les llums i efectes de post processat de la càmara.


